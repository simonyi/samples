/*
 * Project XXXXX
 * 	Copyright text cannot be opened.
 */
package XXXXXXX.core.host.parser.ms.track;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import org.springframework.stereotype.Component;

import XXXXXXXXXXXX.core.host.model.ms.Track1;
import XXXXXXXXXXXX.core.host.model.ms.Track2;
import XXXXXXXXXXXX.core.host.model.ms.TrackData;

/** MS Track1 Data와 Track2 Data를 FSM(Finite State Machine)을 이용하여 parsing 한다. 
 * Track1 Data와 Track2 Data는 붙어 있다고 가정 한다.
 * @author simonyi
 * @date 2016. 3. 14.
 */
@Component
public class TrackDataParser {
	
	// for thread safe.
	private class BufferSet {
		public char fixed_buffer[] = new char[512];		// block read
		public StringBuilder buffer = new StringBuilder();	// character read
	}
	
	private enum State {
		BEGIN,
		PAN,
		CARDHOLDER,
		EXPIREDATE,
		SERVICECODE,
		DISCREATIONARY1,
		CVV,
		DISCREATIONARY2,
		END
	}
	
	/** 
	 * @author simonyi
	 * @date 2016. 3. 14.
	 */
	public TrackDataParser() {
	}
	
	public TrackData parse(String sourceString) throws TrackDataParseException {
		return parse(sourceString, 0);
	}
	
	public TrackData parse(String sourceString, int skip) throws TrackDataParseException {
		try(Reader reader = new StringReader(sourceString)) {
			if(skip > 0) reader.skip(skip);
			return parse(reader);
		} catch(IOException e) {
			throw new TrackDataParseException(e);
		}
	}
	/**
	 * 주어진 String을 parsing하여 그 결과를 TrackData에 담아 리턴한다.
	 * @author simonyi
	 * @date 2016. 3. 14.
	 * @param reader
	 * @return null 일 수 없고 TrackData.getTrack1(), TrackData.getTrack2() 역시 null 일 수 없다.
	 * @throws TrackDataParseException parsing 에러시.
	 */
	public TrackData parse(final Reader reader) throws TrackDataParseException {
		BufferSet bs = new BufferSet();		// for thread safe.
		TrackData track = new TrackData();
		try {
			finiteStateMachine_track1(reader, track.getTrack1(), bs);
			if(!track.isMsOnly())	// Track1이 P인 경우 track2 데이터가 없다.
				finiteStateMachine_track2(reader, track.getTrack2(), bs);
		} catch (IOException e) {
			throw new TrackDataParseException( e );
		}
		
		return track;
	}
	
	private void finiteStateMachine_track1(final Reader reader, Track1 track1, BufferSet bs) throws IOException, TrackDataParseException {
		State state = State.BEGIN;
		bs.buffer.setLength(0);
		while(state != State.END) {
			switch(state) {
				case BEGIN : state = track1_begin(reader, track1, bs); break;
				case PAN : state = track1_pan(reader, track1, bs); break;
				case CARDHOLDER : state = track1_cardholder(reader, track1, bs); break;
				case EXPIREDATE : state = track1_expiredate(reader, track1, bs); break;
				case SERVICECODE : state = track1_servicecode(reader, track1, bs); break;
				case DISCREATIONARY1 : state = track1_discreationary1(reader, track1, bs); break;
				case CVV : state = track1_cvv(reader, track1, bs); break;
				case DISCREATIONARY2 : state = track1_discreationary2(reader, track1, bs); break;
				case END : break;
			}
		}
	}
	
	private State track1_begin(final Reader reader, Track1 track, BufferSet bs) throws IOException, TrackDataParseException {
		char c = (char)reader.read();
		switch(c) {
			case Track1.BEGIN_CHAR :
				track.setBeginChar(c);
				return State.PAN;
			case Track1.BEGIN_CHAR2 : 	// PP Card
				track.setBeginChar(c);
				return State.END;
			default :
				throw new TrackDataParseException("Track1 should be start with " + Track1.BEGIN_CHAR + " not [" + c + "]");
		}
	}
	
	private State track1_pan(final Reader reader, Track1 track, BufferSet bs) throws IOException, TrackDataParseException {
		char c = (char)reader.read();
		switch(c) {
			case (char)-1 :
			case Track1.END_CHAR :
				new TrackDataParseException("premature end of track1 while parsing [PAN]");
			case Track1.SEPARATOR : 
				track.setPAN(bs.buffer.toString());
				bs.buffer.setLength(0);
				return State.CARDHOLDER;
			default :
				bs.buffer.append(c);
		}
		return State.PAN;
	}
	
	private State track1_cardholder(final Reader reader, Track1 track, BufferSet bs) throws IOException, TrackDataParseException {
		char c = (char)reader.read();
		switch(c) {
			case (char)-1 :
			case Track1.END_CHAR :
				new TrackDataParseException("premature end of track1 while parsing [CARDHOLDER]");
			case Track1.SEPARATOR : 
				track.setCardHolderName(bs.buffer.toString());
				bs.buffer.setLength(0);
				return State.EXPIREDATE;
			default :
				bs.buffer.append(c);
		}
		return State.CARDHOLDER;
	}
	
	private State track1_expiredate(final Reader reader, Track1 track, BufferSet bs) throws IOException, TrackDataParseException {
		int read = reader.read(bs.fixed_buffer, 0, 4);
		if(read < 4) new TrackDataParseException("premature end of track1 while parsing [EXPIREDATE]");
		track.setExpireDate(new String(bs.fixed_buffer, 0, 4));
		return State.SERVICECODE;
	}

	private State track1_servicecode(final Reader reader, Track1 track, BufferSet bs) throws IOException, TrackDataParseException {
		int read = reader.read(bs.fixed_buffer, 0, 3);
		if(read < 3) new TrackDataParseException("premature end of track1 while parsing [SERVICECODE]");
		track.setServiceCode(new String(bs.fixed_buffer, 0, 3));
		return State.DISCREATIONARY1;
	}

	private State track1_discreationary1(final Reader reader, Track1 track, BufferSet bs) throws IOException, TrackDataParseException {
		int tobeRead=0; // PAN에 첫자리에 따라 읽는 길이가 달라진다.
		switch(track.getPAN().charAt(0)-'0') {
			case 5 : tobeRead = 14; break;
			case 4 : case 6 : case 8 : case 9 : tobeRead = 7; break;
			case 3 : tobeRead = 4; break;
			default :
				new TrackDataParseException("Invalid PAN of track1 while parsing [DISCREATIONARY1] - not [" + track.getPAN().charAt(0) + "]");
		}
		int read = reader.read(bs.fixed_buffer, 0, tobeRead);
		if(read < tobeRead) new TrackDataParseException("premature end of track1 while parsing [DISCREATIONARY1]");
		track.setDiscretionary1(new String(bs.fixed_buffer, 0, tobeRead));
		return State.CVV;
	}

	private State track1_cvv(final Reader reader, Track1 track, BufferSet bs) throws IOException, TrackDataParseException {
		int read = reader.read(bs.fixed_buffer, 0, 3);
		if(read < 3) new TrackDataParseException("premature end of track1 while parsing [CVV]");
		track.setCVV(new String(bs.fixed_buffer, 0, 3));
		return State.DISCREATIONARY2;
	}

	private State track1_discreationary2(final Reader reader, Track1 track, BufferSet bs) throws IOException, TrackDataParseException {
		char c = (char)reader.read();
		switch(c) {
			case (char)-1 : // track2 data가 없는경우
			case Track1.END_CHAR : 
				track.setDiscretionary2(bs.buffer.toString());
				bs.buffer.setLength(0);
				return State.END;
			default :
				bs.buffer.append(c);
		}
		return State.DISCREATIONARY2;
	}
}
