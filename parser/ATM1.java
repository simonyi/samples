/*
 * Project XXXXX
 * 	Copyright text cannot be opened.
 */
package XXXXXX.core.host.model.ic;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import XXXXXX.core.host.parser.Info;

/**  
 *
 * @author simonyi
 * @date 2016. 3. 15.
 */
@Component
@Scope("prototype")
public class ATM1 extends IcData {
	public static final String START_CODE = "#"+ ATM1.class.getSimpleName()+"#";
	
	@Info(length=04)
	private String install;
	@Info(length=16)
	private String pin;
	@Info(length=16)
	private String csn;
	@Info(length=20)
	private String appLabel;
	@Info(length=16)
	private String fileInfo;
	@Info(length=26)
	private String name;
	@Info(length=13)
	private String regNo;
	@Info(length=2)
	private Integer companyCodeLength;	// 회사코드 길이
	@Info(field="companyCodeLength")
	private String companyCode;			// 회사 코드
	@Info(length=16)
	private String name;
	@Info(length=2)
	private String cardType;
	@Info(length=4)
	private String serviceCode;
	/** 
	 * @author simonyi
	 * @date 2016. 3. 15.
	 */
	public ATM1() {
	}

	// getters and setters or lombok
}
