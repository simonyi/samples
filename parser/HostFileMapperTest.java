/*
 * Project XXXXX
 * 	Copyright text cannot be opened.
 */
package XXXXXXXXXX.core.host.parser;

import static org.junit.Assert.*;

import org.junit.Test;

import XXXXXXXXXX.core.host.model.BodyHeader;
import XXXXXXXXXX.core.host.model.rf.ECA;

/**  
 *
 * @author simonyi
 * @date 2016. 3. 11.
 */
public class HostFileMapperTest {
	
	private HostFileMapper parser = new HostFileMapper();
	
	@Test
	public void testNormalParse() throws MapperException {
		BodyHeader header = new BodyHeader();
		String src = "DATA CANNOT BE OPENED.";
		parser.map(header, src, "euc-kr", false);
		assertEquals(BodyHeader.BEGIN_CHAR, header.getBeginChar());
		assertEquals("EXPECTED CANNOT BE OPENED", header.getPan());
		assertEquals("EXPECTED CANNOT BE OPENED", header.getProductCode());
		assertEquals("EXPECTED CANNOT BE OPENED", header.getChipCode());
		assertEquals("EXPECTED CANNOT BE OPENED", header.getCardDispatchNo());
		assertEquals("EXPECTED CANNOT BE OPENED", header.getMainSubCode());
		assertEquals("EXPECTED CANNOT BE OPENED", header.getRegNo());
		assertEquals("EXPECTED CANNOT BE OPENED", header.getStockCardCode());
		assertEquals("EXPECTED CANNOT BE OPENED", header.getChipKindCode());
	}

	// tons of tests here.

}
