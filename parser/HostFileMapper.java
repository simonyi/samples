/*
 * Project XXXXX
 * 	Copyright text cannot be opened.
 */
package XXXXXXXX.core.host.parser;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.nio.charset.Charset;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.obvioustraverse.dish.util.StringUtils;

import XXXXXXXX.security.hcc.IHCCEncryptor;

/**  
 * Host File data string to Object mapper.
 * filed 에 @Info annotation 처리된 object에 대해 데이터 파일을 읽어 값을 셋팅 해 준다.
 * Object가 계승받은 child class 의 object라면 super class를 찾아 먼저 처리 한다.
 * 따라서 데이터 파일의 순서는 반드시 최상위 클래스로부터 자식 클래스 순으로 순서대로 정의 되어야 한다.
 * 
 * @author simonyi
 * @date 2015. 9. 9.
 */
@Component
public class HostFileMapper {
	
	public static final int FIXED_BUFFER_SIZE = 512;

	@Autowired(required=true) private IHCCEncryptor encryptor;
	
	public HostFileMapper() {
		
	}
	
	/**
	 * default character set, non encrypt 로 mapping 한다.
	 * map(object, sourceString, Charset.defaultCharset().name(), false ); 와 동일 하다.
	 * @author simonyi
	 * @date 2016. 4. 28.
	 * @param object
	 * @param sourceString
	 * @throws MapperException
	 */
	public void map(Object object, String sourceString) throws MapperException {
		map(object, sourceString, Charset.defaultCharset().name(), false );
	}
	/**
	 * source string을 읽어 obj 에 셋팅한다.
	 * @author simonyi
	 * @date 2016. 3. 11.
	 * @param object 세팅될 오브젝트 @Value annotation 처리 되어 있어야 한다.
	 * @param sourceString 데이터 소스
	 * @param fileEncoding file.encoding. eg) ms949 //euc-kr
	 * @param encrypted 이 데이터는 암호화 된 부분이 있다.
	 * @throws MapperException
	 */
	public void map(Object object, String sourceString, String fileEncoding, boolean encrypted) throws MapperException {
		if(object == null) throw new MapperException("obj cannot be null.");
		if(StringUtils.isEmptyOrWhitespace(sourceString)) throw new MapperException("string cannot be null.");
		try(InputStream is = new ByteArrayInputStream(sourceString.getBytes(fileEncoding))) {
			map(object, is, fileEncoding, encrypted);
		} catch (IOException e) {
			throw new MapperException("reader close error", e); // never going to be happened.
		}
	}
	
	/**
	 * source string을 읽어 obj 에 셋팅한다.
	 * @author simonyi
	 * @date 2016. 3. 11.
	 * @param obj obj 세팅될 오브젝트 @Value annotation 처리 되어 있어야 한다.
	 * @param is input stream. byte 단위 조작이 필요하여 Reader를 사용할 수 없다.
 * 	 * @param fileEncoding file.encoding. eg) ms949 //euc-kr
	 * @param encrypted 이 데이터는 암호화 된 부분이 있다.
	 * @throws MapperException
	 */
	public void map(Object obj, InputStream is, String fileEncoding, boolean encrypted) throws MapperException {
		map(obj.getClass(), obj, is, fileEncoding, encrypted);
	}

	/**
	 * current class 부터 recursive하게 superclass를 모두 뒤진다.
	 * @author simonyi
	 * @date 2016. 3. 15.
	 * @param clazz current class
	 * @param obj obj 세팅될 오브젝트 @Value annotation 처리 되어 있어야 한다.
	 * @param is input stream. byte 단위 조작이 필요하여 Reader를 사용할 수 없다.
	 * @param fileEncoding file.encoding. eg) ms949 //euc-kr
	 * @param encrypted 이 데이터는 암호화 된 부분이 있다.
	 * @throws MapperException
	 */
	protected void map(Class<?> clazz, Object obj, InputStream is, String fileEncoding, boolean encrypted) throws MapperException {
		Class<?> superClass = clazz.getSuperclass();
		if(superClass != null){
			 map(superClass, obj, is, fileEncoding, encrypted);
		}
		Field[] fields = clazz.getDeclaredFields();
		byte []buffer = new byte[FIXED_BUFFER_SIZE];
		if(fields != null) {
			for(int i = 0; i < fields.length; i++) {
				fields[i].setAccessible(true);
				Info info = fields[i].getAnnotation(Info.class);
				if(info != null) {
					int tobeRead = resolveLength(info, obj, encrypted);
					if(tobeRead == 0) continue;
					if(tobeRead > FIXED_BUFFER_SIZE) throw new MapperException("Field length cannot exceed " + FIXED_BUFFER_SIZE + " long. Field name = [" + fields[i].getName() + "]");
					try {
						int readLength = is.read(buffer, 0, tobeRead);
						if(readLength != tobeRead) throw new MapperException("premature data end. name = [" + obj.getClass().getSimpleName()+ "." + fields[i].getName() + "], required length = " + tobeRead + ", readLength = " + readLength);
						setValue(obj, fields[i], buffer, tobeRead, fileEncoding, encrypted && info.encryptable());
					} catch (IOException e) {
						throw new MapperException("read error", e);
					} catch (IllegalArgumentException e) {
						throw new MapperException("IllegalArguement", e);
					} catch (IllegalAccessException e) {
						throw new MapperException("Illegal Access", e);
					}
				}
			}
		}
	}

	protected int resolveLength(Info info, Object obj, boolean encrypted) {
		if(StringUtils.isEmpty(info.field())) {
			return (encrypted && info.encryptable()) ? info.encryptedLength() : info.length();
		} else {
			try {
				for (Field field : obj.getClass().getDeclaredFields()) {
					field.setAccessible(true);
					if(field.getName().equals(info.field())) {
					    Object value = field.get(obj); 
					    if (value != null) {
					    	if(value instanceof String) return Integer.parseInt((String)value);
					    	else if(value instanceof Integer) return (Integer)value;
					    }
					}
				}
			} catch(Exception e) {
				// discard;
			}
		}
		return info.length();
	}
	/**
	 * 현재 읽어낸 값으로 field 값을 setting 한다.
	 * @author simonyi
	 * @date 2016. 3. 11.
	 * @param obj setting 해야 하는 Object
	 * @param field setting 해야 하는 field
	 * @param buffer 현재 읽어낸 값을 담은 버퍼
	 * @param length buffer에 담겨  데이터 있는 길이
	 * @param encrypted 이 데이터는 암호화 되어 있다. 필드 타입이 String 인 경우만 적용된다.
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws UnsupportedEncodingException 이런게 발생하면 정말 곤란하다.
	 */
	private void setValue(Object obj, Field field, byte[] buffer, int length, String fileEncoding, boolean encrypted) throws IllegalArgumentException, IllegalAccessException, UnsupportedEncodingException {
		if(field.getType() == String.class ) {
			field.set(obj, encrypted ? encryptor.decrypt(new String(buffer,0,length, fileEncoding)) : new String(buffer,0,length,fileEncoding));
		} else if(field.getType() == Integer.class || field.getType() == Integer.TYPE) {
			field.set(obj, Integer.valueOf(new String(buffer, 0, length)));
		} else if(field.getType() == Character.class || field.getType() == Character.TYPE) {
			field.set(obj, (char)buffer[0]);
		}
	}
}
