/*
 * Project XXXXX
 * 	Copyright text cannot be opened.
 */
package XXXXXXXX.core.script;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.script.Bindings;
import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.Invocable;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.obvioustraverse.dish.lang.type.IEnumBase;
import com.obvioustraverse.dish.smartcard.tag.TLV;
import com.obvioustraverse.dish.smartcard.tag.Tag;
import com.obvioustraverse.dish.util.StringUtils;

import XXXXXXXX.core.script.scriptable.RunScriptException;
import XXXXXXXX.core.script.scriptable.ScriptService;

/**  
 * Nashorn based script engine wrapper.
 * Java 7에서 쓰려면 별도로 빌드된 nashorn-java7.jar 가 필요하며 VM parameter : -Xbootclasspath/a:/path/to/nashorn-java7.jar 가 필요하다.
 * 스크립트를 통한 카드의 발급을 위해 데이터가 세가지 경로로 필요하다.
 * 1. 호스트 전문을 parsing하여 generating 한 데이터.
 * 2. Application profile의 data element에 정의된 정적 데이터
 * 1번과 2번은 합쳐서 P3라고 하고 customization message라는 이름으로 미리 JSON format으로 빌드하여 DB에 넣어둔다.
 * 3. 스크립트 실행 도중 실시간으로 얻거나 연산한 데이터 (Script Service에 정의되어 있다)
 * 
 * 스크립트 엔진은 eval 시마다 새롭게 생성되어야 한다(prototype).
 * 이미 이전 스크립트의 context가 남아 있으면 안되기 때문이다.
 * 
 * @author simonyi
 * @date 2015. 10. 22.
 */
@Component
@Scope("prototype")
public class JavaScriptEngine {
	private static final String LIBRARY_RESOURCE = JavaScriptEngine.class.getPackage().getName().replace('.', '/') + "/lib/default-library.js";
	
	private ScriptEngineManager manager;
	private ScriptEngine engine;
	public static final Scope GLOBAL_SCOPE = Scope.GLOBAL;
	public static final Scope ENGINE_SCOPE = Scope.ENGINE;

	@Autowired(required=true) private ScriptService scriptService;
	@Autowired(required=true) private EmpClientFactory empClientFactory;
	/** 
	 * @author simonyi
	 * @date 2015. 10. 22.
	 */
	public JavaScriptEngine() {
		manager = new ScriptEngineManager();
		engine = manager.getEngineByName("nashorn");
		if(engine == null) throw new IllegalStateException("getEngineByName(\"nashorn\") failed.");

	}

	/**
	 * default TerminalFactory, default CardFactory로 엔진을 초기화 한다.
	 * @author simonyi
	 * @date 2015. 11. 5.
	 * @throws CardException
	 * @throws ScriptException
	 * @throws IOException
	 */
	public void initEngine() throws ScriptException, IOException {
		Map<String, Object> objectBindings = new HashMap<>();
		Map<String, Class<?>> classBindings = new HashMap<>();

		classBindings.put("BERTLV", TLV.class);							// ttk uses ber tlv parser.
		classBindings.put("Tag", Tag.class);							// ttk uses ber tlv parser.
		
		objectBindings.put("service", scriptService);					// 스크립트 실행 동중 동적으로 얻어낼 데이터
		// more bindings here.

		initEngine(objectBindings, classBindings);
	}
	
	/**
	 * 주어진 parameter로 엔진을 초기화 한다.
	 * @author simonyi
	 * @date 2015. 11. 5.
	 * @param factory
	 * @param objectBindings object bindings. eg) ("myObject", new MyClass())
	 * @param classBindings class bindings. eg) ("MyClass", MyClass.class)
	 * @throws CardException
	 * @throws ScriptException
	 * @throws IOException
	 */
	public void initEngine(Map<String,Object> objectBindings, Map<String, Class<?>> classBindings) throws ScriptException, IOException {
		
		Bindings newBindings = engine.createBindings();
		
		// user object binding here.
		addObjectBindings(newBindings, objectBindings);
		engine.setBindings(newBindings, ScriptContext.ENGINE_SCOPE);
		// exit 와 quit 함수는 Java VM전체를 종료시키는 무서운 function이다.
		engine.put("exit", null);
		engine.put("quit", null);
		// default class binding here.
		bindClass("RunScriptException", RunScriptException.class);
		
		// user class binding here
		addClassBindings(classBindings);
		
		// load library.
		loadLibrariesFromResource(LIBRARY_RESOURCE);
	}
		
	protected void addObjectBindings(Bindings bindings, Map<String, Object> objectBindings) {
		if(objectBindings != null)
			bindings.putAll(objectBindings);
	}
	
	protected void addClassBindings(Map<String, Class<?>> classBindings) throws ScriptException {
		if(classBindings != null) {
			for(Map.Entry<String, Class<?>> entry : classBindings.entrySet()) {
				bindClass(entry.getKey(), entry.getValue());
			}
		}
	}
	
	protected void bindClass(String name, Class<?> clazz) throws ScriptException {
		StringBuilder buffer = new StringBuilder();
		buffer.append("var ").append(name).append(" = " )
			.append("Java.type('").append(clazz.getCanonicalName()).append("');");
		engine.eval(buffer.toString());
	}

	public void loadLibrariesFromResource(String ... resources) throws ScriptException, IOException {
		if(resources != null) {
			for(String resource : resources) {
				InputStream is = getClass().getClassLoader().getResourceAsStream(resource);
				if(is == null) throw new IOException("resource [" + resource + "] not found.");
				try(Reader reader = new InputStreamReader(is)) {
					engine.put(ScriptEngine.FILENAME, resource);
					engine.eval(reader);
				} finally {
					engine.put(ScriptEngine.FILENAME, null);
				}
			}
		}
	}
	
	public CompiledScript compileAndEval(Reader reader) throws ScriptException {
		CompiledScript cs = ((Compilable)engine).compile(reader);
		cs.eval();
		return cs;
	}
	
	public Object invokeFunction(String function, Object ...args) throws NoSuchMethodException, ScriptException {
		return ((Invocable)engine).invokeFunction(function, args);
	}
	
	public void runScriptString(String script) throws ScriptException, IOException, RunScriptException {
		try(Reader reader = new StringReader(script)) {
			evaluateScript(reader);
		}
	}

	public void runScriptString(String script, String fileName) throws ScriptException, IOException, RunScriptException {
		try(Reader reader = new StringReader(script)) {
			evaluateScript(reader, fileName);
		}
	}
	
	public Object evaluateScript(Reader reader, String fileName, Bindings bindings) throws RunScriptException, ScriptException {
		try {
			if(StringUtils.isNotEmpty(fileName))
				engine.put(ScriptEngine.FILENAME, fileName);
			if(bindings != null)
				return engine.eval(reader, bindings);
			else
				return engine.eval(reader);
		} catch (ScriptException e) {
			if(e.getCause() != null && e.getCause().getCause() != null) {
				if(e.getCause().getCause() instanceof RunScriptException) {
					throw (RunScriptException)e.getCause().getCause();
				}
			}
			throw e;
		} finally {
			engine.put(ScriptEngine.FILENAME, null);
		}
	}
	
	public static enum Scope implements IEnumBase<Integer> {
		GLOBAL(ScriptContext.GLOBAL_SCOPE, "Global Scope"),
		ENGINE(ScriptContext.ENGINE_SCOPE, "Engine Scope");
		
		private Integer code;// this value will stored to persistent like DB. mandatory.
		private String description; // this value is for just help to understand meaning to developers. mandatory.
		
		Scope(int code, String description) {
			this.code = code;
			this.description = description;
		}

		@Override
		public Integer getCode() {
			return code;
		}

		@Override
		public String getDescription() {
			return description;
		}
		
		private static Map<Integer, Scope> map = IEnumBase.Helpers.createValuesMap(Scope.values());
		public static Scope valueOfCode(String code) {
			return map.get(code);
		}
	}
}
