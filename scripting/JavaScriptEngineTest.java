/*
 * Project XXXXX
 * 	Copyright text cannot be opened.
 */
package XXXXXXXXX.hscms.core.script;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Security;

import javax.script.ScriptException;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import XXXXXXXXX.core.script.scriptable.RunScriptException;

/**  
 * this is not a Spring test.
 * service will be null.
 * 
 * @author simonyi
 * @date 2015. 10. 22.
 */
public class JavaScriptEngineTest {
	public static final String HERE = "test/" + JavaScriptEngineTest.class.getPackage().getName().replace('.', '/');
	
	public static final String TEST_FILE_EXCEPTION = HERE + "/test-exception.js";

	private JavaScriptEngine engine;

	@BeforeClass
	public static void beforeClass() {
		Security.addProvider(new BouncyCastleProvider());
	}
	
	@Before
	public void before() throws ScriptException, IOException {
		engine = new JavaScriptEngine();
		engine.initEngine();
	}
	
	@Test(expected=IOException.class)
	public void testExceptionCase() throws FileNotFoundException, IOException, ScriptException, RunScriptException {
		engine.runScriptResource("ResourceNowhere");
	}
	
	@Test(expected=RunScriptException.class)
	public void testScriptException() throws Throwable {
		engine.runScriptFile(TEST_FILE_EXCEPTION);
	}
	
	@Test
	public void testEval() throws ScriptException, IOException, RunScriptException {
		engine.runScriptString("print(new Array(10+1).join('B'))");
	}

}
