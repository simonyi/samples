* Embedded Script Engine
  * 수시로 변경되는 비지니스 로직은 스크립트로 작성하여 서버의 중단없이 스크립트를 업로드 함으로서 비지니스 로직을 변경할 수 있고, 비 개발자가 간단한 스크립팅으로 비지니스 로직은 직접 만들 수 있게 지원함.