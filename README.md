# simonyi's coding samples.

* 주요 작업에 대한 코딩 스타일을 공유 합니다.
* 프로젝트에서 일부 코드만을 가져온것이기 때문에 빌드와 실행의 보장은 없습니다.
* 비밀유지각서 때문에 공개할 수 있는게 별로 없습니다. 양해 바랍니다.

## Focas Javadoc
* 소스코드는 공개할 수 없고, 대신 [javadoc](focas-0.1.0-SNAPSHOT-javadoc.jar) 만 공개 합니다.

## Contribute
![2020-RNR](./2020-RNR-gitlab.png)
![2021-RNR](./2021-RNR-gitlab.png)
![2022-RNR](./2022-RNR-gitlab.png)
![2023-RNR](./2023-RNR-gitlab.png)
