/*
 */
package XXXXXXX.devrelay.client.db;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class RelayImpl implements IDBEncryptor {
	private final String baseUrl;

	private final URL urlEnc;
	private final URL urlDec;

	public RelayImpl(String config) {
		if(config.endsWith("/")) {
			baseUrl = config.substring(0, config.length() - 1);		// trailing end '/'
		} else {
			baseUrl = config;
		}
		try {
			urlEnc = new URL(baseUrl + "/enc");
			urlDec = new URL(baseUrl + "/dec");
		} catch(MalformedURLException ex) {
			throw new RuntimeException("Malformed URL : " + config, ex);
		}
	}

	protected String doPost(URL url, String data) {
		try {
			byte[] postData = ("data=" + URLEncoder.encode(data != null ? data : "", StandardCharsets.UTF_8.name())).getBytes(StandardCharsets.UTF_8);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setConnectTimeout(3000); //3 seconds timeout
			connection.setReadTimeout(2000); //2 seconds timeout
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			connection.setRequestProperty("Content-Length", String.valueOf(postData.length));
			connection.setDoInput(true);
			connection.setDoOutput(true);
			OutputStream os = connection.getOutputStream();
			os.write(postData);
			os.flush();
			os.close();
		
			int httpStatus = connection.getResponseCode();
			if(HttpURLConnection.HTTP_OK == httpStatus) {
				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				String result = in.readLine();
				in.close();
				return result;
			}
			throw new RuntimeException("server invalid status : " + httpStatus);
		} catch (Exception e) {
			throw new RuntimeException("relay server communication error.", e);
		}
	}
	
	@Override
	public String enc(String data) {
		return doPost(urlEnc, data);
	}

	@Override
	public String dec(String encData) {
		return doPost(urlDec, encData);
	}
}
