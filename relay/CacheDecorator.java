/*
 */
package XXXXXXX.devrelay.client.db.cache;

import java.util.Collections;
import java.util.Map;

import XXXXXXX.devrelay.client.db.IDBEncryptor;

/**  
 * 캐시를 사용할 경우 이 decorator로 감싸주면 cache를 이용한 구현으로 변경된다.
 */
public class CacheDecorator implements IDBEncryptor {
	private final IDBEncryptor delegate;		// source encrytor.
	private final ICache encCache = CacheFactory.get("encCache");

	public CacheDecorator(IDBEncryptor delegate) {
		if(delegate instanceof CacheDecorator) throw new IllegalStateException("Cannot accept decorator in decorator.");
		this.delegate = delegate;
	}

	@Override
	public String enc(String data) {
		if(encCache.get(data) != null) return encCache.get(data);
		String result = delegate.enc(data);
		encCache.put(data, result);
		return result;
	}

	@Override
	public String dec(String encData) {
		if(encCache.get(encData) != null) return encCache.get(encData);
		String result = delegate.dec(encData);
		encCache.put(encData, result);
		return result;
	}
}
