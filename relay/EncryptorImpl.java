/**
 * 
 */
package XXXXXXX.devrelay.client.db;

import YYYYYY.RealClient;

public class EncryptorImpl implements IDBEncryptor {
	private final RealClient agt = new RealClient();
	private final String iniFile; 
	
	public EncryptorImpl(String iniFile) {
		this.iniFile = iniFile;
	}
	
	@Override
	public String enc(String plainText) {
		return agt.ScpEncB64(iniFile, "KEY1", plainText, "EUC-KR");
	}
	
	@Override
	public String dec(String encrypted) {
		return agt.ScpDecB64(iniFile, "KEY1", encrypted, "EUC-KR");
	}
}
