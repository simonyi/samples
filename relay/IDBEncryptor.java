/**
 * 
 */
package XXXXXXX.devrelay.client.db;

import XXXXXXX.devrelay.client.db.CacheDecorator;

/**
 * main interface.
 *
 */
public interface IDBEncryptor {

	/***** 전체 암호화 *****/
	String enc(String data);

	/***** 전체 복호화 *****/
	String dec(String encData);

	/**
	 * 구현체의 종류를 선택한다. 
	 *   PLAIN - 암호화를 하지 않고 그대로 돌려주는 구현체.
	 *   DIRECT - 디아모 라이브러리를 직접 사용하여 암호화를 하는 구현체. config 로서 iniFile path를 주어야 한다.
	 *   RELAY - dev-relay 서버를 경유하여 암/복호화를 하는 구현체. relay서버가 정상 수행 중이어야한다. config 로서 relay server baseUrl 을 주어야 한다.
	 */
	public enum EImpl {
		PLAIN,
		DIRECT,
		RELAY
	}
	
	/**
	 * 구현체를 얻는다.
	 *   PLAIN - 암호화를 하지 않고 그대로 돌려주는 구현체.
	 *   DIRECT - 라이브러리를 직접 사용하여 암호화를 하는 구현체. config 로서 iniFile path를 주어야 한다.
	 *   RELAY - dev-relay 서버를 경유하여 암/복호화를 하는 구현체. relay서버가 정상 수행 중이어야한다. config 로서 relay server baseUrl 을 주어야 한다.
	 */
	public static class Factory {
		private String config = "";
		private boolean enableCache = false;
		
		public static Factory withConfig(String config) {
			Factory factory = new Factory();
			factory.config = config;
			return factory;
		}

		public static Factory autoDetect() {
			return new Factory();
		}
		
		public Factory enableCache(Boolean enableCache) {
			this.enableCache = enableCache;
			return this;
		}
		
		public IDBEncryptor getInstance(EImpl impl) {
			IDBEncryptor src = null;
			switch(impl) {
				case DIRECT :
					src =  new EncryptorImpl(config);
					break;
				case RELAY :
					src = new RelayImpl(config);
					break;
				default :
					src = new NullImpl();
					break;
			}
			return enableCache ? new CacheDecorator(src) : src;
		}

		public static IDBEncryptor getInstance(EImpl impl, String config) {
			return Factory.withConfig(config).getInstance(impl);
		}
	}
}