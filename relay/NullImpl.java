/**
 * 
 */
package XXXXXXX.devrelay.client.db;

/** 암호화를 수행하지 않는 encryptor. 테스트용.
 */
public class NullDBEncryptorImpl implements IDBEncryptor {

	@Override
	public String enc(String data) {
		return data;
	}

	@Override
	public String dec(String encData) {
		return encData;
	}
}
