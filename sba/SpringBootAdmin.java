package XXXXXXXXXX.server.monitor;

import java.util.Collections;
import java.util.List;

import javax.net.ssl.SSLException;

import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.netflix.turbine.EnableTurbine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

import de.codecentric.boot.admin.server.config.AdminServerProperties;
import de.codecentric.boot.admin.server.config.EnableAdminServer;
import de.codecentric.boot.admin.server.domain.entities.InstanceRepository;
import de.codecentric.boot.admin.server.web.client.HttpHeadersProvider;
import de.codecentric.boot.admin.server.web.client.InstanceExchangeFilterFunction;
import de.codecentric.boot.admin.server.web.client.InstanceWebClient;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import reactor.netty.http.client.HttpClient;


@Configuration
@EnableAutoConfiguration
@EnableAdminServer
public class SpringBootAdmin {
	@Autowired private AdminServerProperties adminServerProperties;
	
	/*
	 * self-signed certificate를 인증하고 host name verify를 skip 한다. 
	 */
	@Bean
	@ConditionalOnProperty(name="security.trust.self-signed", havingValue="true", matchIfMissing=false)
	public InstanceWebClient instanceWebClient(HttpHeadersProvider httpHeadersProvider, ObjectProvider<List<InstanceExchangeFilterFunction>> filtersProvider) throws SSLException {
		SslContext sslContext = SslContextBuilder.forClient().trustManager(InsecureTrustManagerFactory.INSTANCE).build();
		HttpClient httpClient = HttpClient.create().secure(t->t.sslContext(sslContext));
    	     	 
        List<InstanceExchangeFilterFunction> additionalFilters = filtersProvider.getIfAvailable(Collections::emptyList);
        return InstanceWebClient.builder()
			.connectTimeout(adminServerProperties.getMonitor().getConnectTimeout())
			.readTimeout(adminServerProperties.getMonitor().getReadTimeout())
			.defaultRetries(adminServerProperties.getMonitor().getDefaultRetries())
			.retries(adminServerProperties.getMonitor().getRetries())
			.httpHeadersProvider(httpHeadersProvider)
			.filters(filters -> filters.addAll(additionalFilters))
			.webClientCustomizer(builder->{
				builder.clientConnector(new ReactorClientHttpConnector(httpClient));
			})
			.build();
    }
	public static void main(String[] args) {
		SpringApplication.run(SpringBootAdmin.class, args);
	}
}

